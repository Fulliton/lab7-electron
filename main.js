"use strict";
const electron = require("electron");
var crypt = require("./crypt.js"); //Кастом либа для шифрования по ключу
const fs = require("fs");
const path = require("path");
const url = require("url");

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
const dialog = electron.dialog;
const ipc = electron.ipcMain;

var protocolFile; //данные с протокола, глобал перем.

var name, group;

var mainWindow, firstWindow, protocolWindow, theoryWindow;

const template = [
  {
    label: "Просмотр",
    submenu: [
      {
        label: "Сброс размера окна",
        role: "resetzoom"
      },
      {
        label: "Приблизить",
        role: "zoomin"
      },
      {
        label: "Отдалить",
        role: "zoomout"
      },
      {
        type: "separator"
      },
      {
        label: "В полный экран",
        role: "togglefullscreen"
      }
    ]
  },
  {
    label: "Окно",
    role: "window",
    submenu: [
      {
        label: "Свернуть",
        role: "minimize"
      },
      {
        label: "Закрыть",
        role: "close"
      }
    ]
  },
];

if (process.platform === "darwin") {
  const name = app.getName();
  template.unshift({
    label: name,
    submenu: [
      {
        role: "about"
      },
      {
        type: "separator"
      },
      {
        role: "services",
        submenu: []
      },
      {
        type: "separator"
      },
      {
        role: "hide"
      },
      {
        role: "hideothers"
      },
      {
        role: "unhide"
      },
      {
        type: "separator"
      },
      {
        role: "quit"
      }
    ]
  });
  // Edit menu.
  template[1].submenu.push(
    {
      type: "separator"
    },
    {
      label: "Speech",
      submenu: [
        {
          role: "startspeaking"
        },
        {
          role: "stopspeaking"
        }
      ]
    }
  );
  // Window menu.
  template[3].submenu = [
    {
      label: "Close",
      accelerator: "CmdOrCtrl+W",
      role: "close"
    },
    {
      label: "Minimize",
      accelerator: "CmdOrCtrl+M",
      role: "minimize"
    },
    {
      label: "Zoom",
      role: "zoom"
    },
    {
      type: "separator"
    },
    {
      label: "Bring All to Front",
      role: "front"
    }
  ];
}

const menu = Menu.buildFromTemplate(template);

function createWindow() {
  // Create the browser window.
  firstWindow = new BrowserWindow({
    width: 600,
    height: 250,
    resizable: false,
    center: true,
    title: "Регистрация",
    show: false,
    // frame: false,
  }); //Окно с вводом фио и группой

  // and load the index.html of the app.
  firstWindow.loadURL(url.format({
    pathname: path.join(__dirname, "src/fio.html"),
    protocol: "file:",
    slashes: true
  }));

  Menu.setApplicationMenu(null);
//  firstWindow.webContents.openDevTools();

  firstWindow.once("ready-to-show", function() {
    firstWindow.show();
  });

  // Emitted when the window is closed.
  firstWindow.on("closed", function () {
    firstWindow = null;
  });
}

/*
  Ждём запроса от окна fio, создаём окно с самой лабой.
  На запрос получаем данные с полей, имя и группа
*/
ipc.on("open-mainWindow", function (event, tName, tGroup) {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    minWidth: 800,
    minHeight: 400,
    show: false,
    // frame: false,
  });
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, "src/index.html"),
    protocol: "file:",
    slashes: true
  }));
  mainWindow.on("closed", function () {
    mainWindow = null;
  });
  mainWindow.once("ready-to-show", function() {
    mainWindow.show();
  });
  // mainWindow.webContents.openDevTools();
  Menu.setApplicationMenu(menu);
  name = tName;
  group = tGroup;
});

ipc.on("get-fio-group", function (event) {
  event.sender.send("get-fio-group-reply", name, group);
});


/*
  Ждём запроса от окна fio что бы открыть диалок для чтения файла,
  создаём новое окно(протокола) и закрываем окно fio.
*/
ipc.on("open-protocol", function() {
  dialog.showOpenDialog({ filters: [{ name: "Lab", extensions: ["lab"] }] }, function(fileNames) {
    if(fileNames === undefined) {
      
    }
    else {

      var fileName = fileNames[0];

      fs.readFile(fileName, "utf-8", function(err, data) {
        if(err) {
          fileName = null;
          fileNames = null;
          dialog.showErrorBox("Ошибка", "Невозможно открыть файл");
        }
        else {
          protocolFile = JSON.parse(crypt.decrypt(data));
          if(protocolFile.user.labID === "lab7") {
            //Создаём окно протокола
            protocolWindow = new BrowserWindow({
              width: 800,
              height: 600,
              minWidth: 600,
              minHeight: 400,
              show: false,
              // frame: false,
            });
            protocolWindow.loadURL(url.format({
              pathname: path.join(__dirname, "src/protocol.html"),
              protocol: "file:",
              slashes: true
            }));
            protocolWindow.on("closed", function () {
              protocolWindow = null;
            });
            // protocolWindow.webContents.openDevTools();
            Menu.setApplicationMenu(menu);
            protocolWindow.once("ready-to-show", function () {
              protocolWindow.show();
            });
            firstWindow.close();
            fileName = null;
            fileNames = null;
          }
          else {
            protocolFile = null;
            dialog.showErrorBox("Ошибка", "Извините, этот протокол не соответствует данной программе");
          }
        }
      });
    }
  });
});


/*
  Ждём запроса от окна протокола что бы
  отправить данные с протокола, см выше.
*/
ipc.on("window-protocol-show", function(event) {
  event.sender.send("window-protocol-show-reply", protocolFile);
  name = null;
  group = null;
});


/*
  Ждём запроса для закрытие всех
  окон и перехеда в окно fio
*/
ipc.on("go-to-fio", function() {
  createWindow();
  name = null;
  group = null;
});


/*
  Ждём запроса от окна mainWindow
  для открытие нового окна с теорией,
  окно модальное, переход в другие окна запрещено
*/
ipc.on("theory-window-open", function() {
  theoryWindow = new BrowserWindow({
    width: 800,
    height: 600,
    minWidth: 600,
    minHeight: 400,
    show: false,
    parent: mainWindow,
    modal: true,
    // frame: false,
  });
  theoryWindow.loadURL(url.format({
    pathname: path.join(__dirname, "src/theory.html"),
    protocol: "file:",
    slashes: true
  }));
  theoryWindow.on("closed", function () {
    theoryWindow = null;
  });
  theoryWindow.once("ready-to-show", function() {
    theoryWindow.show();
  });
  Menu.setApplicationMenu(menu);
});


/*
  Ждём запроса от окна mainWindow для сохранения протокола.
  Получаем не фишрованный протокол и путь куда сохранить файл lab7.lab.
  Шифруем и записываем.
  Открываем диалоговое окно для подтверждения сохранения. 
*/
ipc.on("save-protocol", function(event, obj, folder) {
  obj = crypt.encrypt(obj);
  fs.writeFile(folder, obj, function (err) {
    if (err) {
      dialog.showErrorBox("Ошибка", "Ошибка сохранения протокола");
    }
    else {
      dialog.showMessageBox(mainWindow, {
        type: "none",
        title: "Уведомление",
        message: "Протокол сохранён",
        buttons: ["Закрыть"]
      });
    }
  });
  obj = null;
});

app.on("ready", createWindow);


// Quit when all windows are closed.
app.on("window-all-closed", function () {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function () {
  if (mainWindow === null) {
    createWindow();
  }
});
