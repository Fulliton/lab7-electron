let canvas = document.getElementById("pendulum");
let ctx = canvas.getContext("2d");
let animationID;
let animationStart;
let animationTimes = null;
let currentTime = 0;
let realCurrentTime = 0;
let solidX = 150;
let solidY = 80;
let offsetY = 2;
let offsetX = 0;
let counter = 0;
let rotateSpeed = 6;

let reversed = false;
let end = false;
let dx = 1;

let startButton;
let resetButton;

canvas.width = 250;
canvas.height = 400;

ctx.lineWidth = 2;

window.onload = function() {
  startButton = document.getElementById("start");
  resetButton = document.getElementById("reset");

  startButton.disabled = true;
  resetButton.disabled = true;

  startButton.onclick = function() {
    if (counter === 0) {
      animationStart = Date.now();
      animation();
      startButton.disabled = true;
      resetButton.disabled = true;
    }
  };

  resetButton.onclick = function() {
    cancelAnimationFrame(animationID);
    clear();
    animationStart = undefined;
    counter = 0;
    drawPendulum();
    drawSolid(0, 0);
    initNextTime();

    end = false;
    dx = Math.abs(dx);
    reversed = false;

    startButton.disabled = false;
    resetButton.disabled = true;
  };

  drawPendulum();
  drawSolid(0, 0);
};
// just test

function clear() {
  ctx.save();
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.restore();
}

function drawPendulum() {
	ctx.strokeRect(10, 380, 200, 10);// lower beam
	ctx.strokeRect(15, 390, 10, 10);// left leg
	ctx.strokeRect(195, 390, 10, 10);// right leg

	ctx.strokeRect(35, 390, 30, 5);// top rack
	ctx.strokeRect(35, 375, 30, 5);// bot rack
	ctx.strokeRect(40, 40, 20, 335);// rack

	ctx.strokeRect(30, 35, 165, 5);// topper beam

	ctx.strokeRect(145, 40, 10, 5);// bot holder
	ctx.strokeRect(145, 30, 10, 5);// top holder

	ctx.strokeRect(125, 40, 12, 5);// left magnet
	ctx.strokeRect(163, 40, 12, 5);// right magnet

	ctx.beginPath();
	//left magnit
	ctx.moveTo(127, 45);
	ctx.lineTo(127, 55);
	ctx.lineTo(135, 50);
	ctx.lineTo(135, 45);

	//right magnit
	ctx.moveTo(173, 45);
	ctx.lineTo(173, 55);
	ctx.lineTo(165, 50);
	ctx.lineTo(165, 45);
	ctx.stroke();
	ctx.closePath();

	ctx.strokeRect(80, 330, 100, 50);
	ctx.strokeRect(90, 340, 80, 30);

	ctx.beginPath();

	for (let i = 0; i < 241; i += 3) {
		if ((i / 3) % 5 === 0) {
			ctx.moveTo(60, 70 + i);
			ctx.lineTo(50, 70 + i);
		} else {
			ctx.moveTo(60, 70 + i);
			ctx.lineTo(55, 70 + i);
		}
	}

	ctx.stroke();
	ctx.closePath();

	ctx.font = "bold 14pt Arial";

	if (!end) {
		if (Date.now() - animationStart) {
			if (Date.now() - animationStart < currentTime * 1000) {
				ctx.fillText((Date.now() - animationStart) / 1000 + "s", 100, 362);
			} else {
				ctx.fillText(currentTime + "s", 100, 362);
			}
		} else {
			ctx.fillText("0s", 100, 362);
		}
	} else {
		ctx.fillText(realCurrentTime + "s", 100, 362);
	}
}

function drawSolid(offsetX, offsetY) {
  ctx.beginPath();
  ctx.translate(solidX + offsetX, solidY + offsetY);
  ctx.rotate(counter * rotateSpeed * Math.PI / 180);
  ctx.arc(0, 0, 30, 0, 360, false);
  ctx.arc(0, 0, 5, 0, 360, false);
  ctx.rotate(-counter * rotateSpeed * Math.PI / 180);
  ctx.translate(-solidX - offsetX, -solidY - offsetY);
  ctx.moveTo(solidX + offsetX, solidY + offsetY - 5);
  ctx.lineTo(solidX, 45);
  ctx.stroke();
  ctx.closePath();
  // console.log(Date.now() - animationStart + " : " + currentTime * 1000);
  if (Date.now() - animationStart > currentTime * 1000) {
    if (animationTimes.indexOf(realCurrentTime) < animationTimes.length - 1) {
      resetButton.disabled = false;
    } else if (!completed) {
	    completed = true;
	    console.log(completed);
    }
    return false;
  }
  return true;
}

function animation() {
  if (animationTimes != null && animationTimes !== undefined) {
    clear();
    if (drawSolid(counter * offsetX, counter * offsetY)) {
      counter += dx;
      animationID = requestAnimationFrame(animation);
    } else if (currentTime * 1000 < 1) {
      dx = Math.abs(dx);
      reversed = false;
	    cancelAnimationFrame(animationID);
	    setTimeout(function() {
	    	end = false;
	    }, 300)
    } else {
    	if (reversed) {
				currentTime = realCurrentTime - (realCurrentTime - currentTime);
				reversed = false;
	    } else {
		    currentTime /= 3;
		    reversed = true;
		    end = true;
	    }

	    dx *= -1;
	    animationStart = Date.now();
	    animationID = requestAnimationFrame(animation);
    }
    drawPendulum();
  } else {
    throw "Animation times don't initialized";
  }
}

function initAnimationTimes(times) {
	animationTimes = [];
  for (let i = 0; i < times.length; i++) {
    animationTimes.push(times[i]);
  }
  console.log(animationTimes);
  currentTime = animationTimes[0];
  realCurrentTime = currentTime;
  offsetY = 200 / (currentTime * 60);
}

function initNextTime() {
  if (animationTimes.indexOf(realCurrentTime) < animationTimes.length - 1) {
    currentTime = animationTimes[animationTimes.indexOf(realCurrentTime) + 1];
	  realCurrentTime = animationTimes[animationTimes.indexOf(realCurrentTime) + 1];
    offsetY = 200 / (currentTime * 60);
  }
}
