const electron = require('electron');
const { dialog } = require('electron').remote;
const fs = require('fs');
const remote = electron.remote;
const ipc = electron.ipcRenderer;

/*
  Формируем имя и группу в перем.
  Отправляем запрос по каналу open-mainWindow с перем name, group
  Закрываем окно fio
*/
function creatMainWindow() {
  var form = document.forms.fio;
  var name = form.elements.name.value;
  var group = form.elements.group.value;
  
  ipc.send('open-mainWindow', name, group);
  var window = remote.getCurrentWindow();
  window.close();
}

/*
  Закрываем окно fio
*/
function closeWindow() {
  var window = remote.getCurrentWindow();
  window.close();
}

/*
  Отправляем запрос по каналу open-protocol
  Для просмора протокола в отдельном окне
*/
function protocol() {
  ipc.send('open-protocol');
}