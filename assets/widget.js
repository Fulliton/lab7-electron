let completed = true;
let answered = true;
let numberExperiment = -1;

let ringParams = {
	0: {
		weight: 0.260,
		maxSpeed: 0.190,
		minSpeed: 0.145,
		used: false
	},
	1: {
		weight: 0.410,
		maxSpeed: 0.244,
		minSpeed: 0.172,
		used: false
	},
	2: {
		weight: 0.630,
		maxSpeed: 0.300,
		minSpeed: 0.260,
		used: false
	}
};

let values = {
	time: {
		t: [], 	// массив времен
		dt: [], 	// массив дельта времен
		avT: undefined		// среднее время
	},
	J: undefined,
	m: null,
	answer: 0,
	errors: 0,
	exp: 0
};

let sqErr = {
	t: 0,
	d: 0,
	h: 0,
	g: 0.005,
	m: 0.004,
	J: 0
};

function getFallSpeed(ringParam) {
	return Math.random() * (ringParam.maxSpeed - ringParam.minSpeed) + ringParam.minSpeed;
}

function chooseRing(index) {
	if (completed && answered && (ringParams[index] !== values.m && !ringParams[index].used)) {
		values.m = ringParams[index];
		values.m.used = true;

		$($(".ring-active")[0]).removeClass("ring-active");
		$($(".ring")[index]).addClass("ring-active");

		values.time.t = [];
		values.time.dt = [];
		values.exp = 0;
		values.errors = 0;

		completed = false;
		answered = false;

		calculatePhysics();
		startButton.disabled = false;
		document.getElementById("check").disabled = false;

		cancelAnimationFrame(animationID);
		clear();
		animationStart = undefined;
		counter = 0;
		drawPendulum();
		drawSolid(0, 0);

		end = false;
		dx = Math.abs(dx);
		reversed = false;

		startButton.disabled = false;
		resetButton.disabled = true;

		if (numberExperiment > -1) {
			for (let [key, element] of valueMap) {
				element.dom.innerHTML = "<input type='text' name='" + key +
					"' class='form-control' placeholder='Введите " + key + "' " +
					"id='" + key + "'>";

				element.dom.classList.remove('has-error');
				element.dom.classList.remove('has-success');
				element.dom.classList.remove('has-feedback');
			}

			valueMap.get("tcp").dom.innerHTML = '<input type="text" name="tcp" ' +
				'class="form-control" placeholder="Введите tcp" id="tcp">';

			valueMap.get("tcp").dom.classList.remove('has-error');
			valueMap.get("tcp").dom.classList.remove('has-success');
			valueMap.get("tcp").dom.classList.remove('has-feedback');

			valueMap.get("J").dom.innerHTML = '<input type="text" class="form-control" id="J" placeholder="J">';

			valueMap.get("J").dom.classList.remove('has-error');
			valueMap.get("J").dom.classList.remove('has-success');
			valueMap.get("J").dom.classList.remove('has-feedback');
		}

		numberExperiment++;
	}
}

function Line() {
	console.log('-----------------------------------------------');
}

function calculatePhysics() {
	const N = 5; // количество опытов
	const h = 0.4; // высота установки Максвелла [м]
	const g = 9.80665; // ускорение свободного падения [м/c^2]
	const d = 0.01; // диаметр оси [м]
	const pt = 2.78; // коэффициент стьюдента при p = 0.95 для 5-ти опытов
	const scaleD = 0.001;
	const scaleH = 0.01;


	for (let i = 0; i < N; i++) {
		values.time.t.push(Number((h / getFallSpeed(values.m)).toFixed(3)));
		console.log('t' + i + ' = ' + values.time.t[i]);
	} // расчитывание времени по каждому запуску [c]

	initAnimationTimes(values.time.t);
	Line();

	values.time.avT = 0;

	for (let i = 0; i < N; i++) {
		values.time.avT += values.time.t[i];
	}
	values.time.avT = Number((values.time.avT / N).toFixed(3));// среднее время [c]

	console.log(values.time.avT);

	for (let i = 0; i < N; i++) {
		values.time.dt.push(Math.round((values.time.t[i] - values.time.avT) * 1000) / 1000);
	} // рассчитывание дельта времени по каждому запуску [c]

	for (let i = 0; i < N; i++) {
		sqErr.t += Math.pow(values.time.dt[i], 2);
	}
	sqErr.t = sqErr.t / (Math.sqrt(N * (N - 1)));
	sqErr.d = scaleD / Math.sqrt(12);
	sqErr.h = scaleH / Math.sqrt(12);

	values.J = (0.25) * values.m.weight * Math.pow(d, 2) * (((g * Math.pow(values.time.avT, 2)) / (2 * h)) - 1);

	sqErr.J = values.J * Math.sqrt(
		Math.pow((2 * sqErr.t) / values.time.avT, 2) +
		Math.pow((2 * sqErr.d) / d, 2) +
		Math.pow(sqErr.m / values.m.weight, 2) +
		Math.pow(sqErr.h / h, 2) +
		Math.pow(sqErr.g / g, 2));

	values.answer = sqErr.J * pt;

	while (values.J < 1) {
		values.J *= 10;
		values.exp++;
	}

	values.J = Number(values.J.toFixed(3));
	document.getElementById("exp").innerHTML = values.exp;

	console.log('J = ' + values.J);
}
