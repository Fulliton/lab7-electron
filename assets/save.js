"use strict";
const electron = require('electron');
const { dialog } = require('electron').remote;
const remote = electron.remote;

let protocolData = {
  user: {
    group: null,
    name: null,
    labID: "lab7"
  },
	experiments: {
    0: {},
    1: {},
    2: {}
  }
};

/*
  Функция по сохранению данных
*/
function save() {

  ipc.send('get-fio-group');

  ipc.on('get-fio-group-reply', function(event, Tname, Tgroup){

      $('#status').fadeIn(); // will first fade out the loading animation 
      $('#preloader').delay(350).fadeIn('slow'); // will fade out the white DIV that covers the website. 

    protocolData.user.name = Tname;
    protocolData.user.group = Tgroup;

    let jsonData = JSON.stringify(protocolData);

    let fileNames = 'lab7.lab';
    dialog.showOpenDialog({
      title: "Выберите путь",
      properties: ["openDirectory"]
    }, function(folderPaths) {
      // folderPaths is an array that contains all the selected paths
      if (folderPaths === undefined) {
        $('#status').fadeOut(); // will first fade out the loading animation 
        $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
        return ; 
      } else {
        folderPaths = folderPaths + "/" + fileNames;
        ipc.send('save-protocol', jsonData, folderPaths);
        $('#status').fadeOut(); // will first fade out the loading animation 
        $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
      }
    });
    $("#t1").prop("disabled", true);
    ipc.removeAllListeners('save-protocol');
    ipc.removeAllListeners('get-fio-group-reply');
    ipc.removeAllListeners('get-fio-group');
  });
}